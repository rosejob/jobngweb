export interface LoginResp {
    success: boolean;
    token?: string;
    message?: string;
    msgCode?: string;
}