import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './page/home/home.component';
import { LoginComponent } from './page/login/login.component';
import { PublicZoneComponent } from './page/public-zone/public-zone.component';
import { RegisterComponent } from './page/register/register.component';

const routes: Routes = [
  {
    path: '',
    component: PublicZoneComponent,
    children: [
      {
  path: 'home',
  component: HomeComponent,
      },

{
  path: 'register',
  component: RegisterComponent,
},

{
  path: 'login',
  component: LoginComponent,
},

{
  path: 'user',
  component: userComponent,
},

    ],
  },

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
