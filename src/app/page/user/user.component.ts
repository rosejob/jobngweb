import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  dataSource = [];
  displayedColumns: string[] = ['code', 'name' , 'email', 'role', 'active'];

  constructor(private userService: UserService) { }

  ngOnInit(): void { 
    this.userService.filter({}).subscribe((resp) => {
      this.dataSource = resp;
    }
    );
   }



}
