import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private fb: FormBuilder, private userService: UserService) { }
  registerForm = this.fb.group ({
    email: ['', [Validators.required, Validators.email]],
    code: ['',[Validators.required, Validators.maxLength(5)]],
    name: ['',[Validators.required, Validators.maxLength(150)]],
    pwd: ['', [Validators.required,Validators.maxLength(15)]],
  });

  ngOnInit(): void {
  }

  onRegisterSubmit(){
    if (this.registerForm.valid) {
      this.userService.createUser(this.registerForm.value).subscribe((resp) => {
          console.log(resp);
        });
    } else {
    }

  }

}
