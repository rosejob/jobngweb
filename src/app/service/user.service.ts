import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

   createUser(user: User){
     return this.http.post(`${environment.apiHost}/user`, user);
   }

   login(login: Login) {
     return this.http.post(`${environment.apiHost}/user/login`, user);
   }

}
