FROM node:14-alpine as builder
WORKDIR /webapp
COPY . /webapp
RUN npm i
RUN npm run ng build -- --prod

FROM nginx:alpine
COPY --from=builder /webapp/dist/jobngweb /usr/share/nginx/html

# docker build -t jobngweb .
# docker image ls
# docker run --name jobngweb -p 81:80 jobngweb
